package fer.oop.Swing2;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
//import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
public class Addition extends JFrame {

	private static final long serialVersionUID = 1L;
	private char lastPressedKey = 'a';
	public static void main(String[] args) {
		/*String firstNum = JOptionPane.showInputDialog("Enter first integer");
		String secondNum = JOptionPane.showInputDialog("Enter second integer");
		
		int sum = Integer.parseInt(firstNum) + Integer.parseInt(secondNum);
		
		JOptionPane.showMessageDialog(null, "Sum is" + sum, "Sum window", JOptionPane.PLAIN_MESSAGE);
		*/
		try {
			SwingUtilities.invokeAndWait(()-> new Addition());
		}
		catch (InterruptedException  | InvocationTargetException e){
			e.printStackTrace();
		}
	}
	
	
	public Addition() {
		setVisible(true);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		Container cp = getContentPane();
		JButton gumb = new JButton("stisni");
		cp.setLayout(new FlowLayout());
		cp.add(gumb);
		gumb.setActionCommand("jurij");
		gumb.addActionListener(l ->{
			System.out.println(l.getSource());
			System.out.println(l.getActionCommand());
		});
		pack();
		gumb.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				Character x = e.getKeyChar();
				if (x.hashCode() == 23) {
					Addition.this.dispose();
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				char x = e.getKeyChar();
				if (x == 'x') {
					gumb.setBackground(Color.BLUE);
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				char x = e.getKeyChar();
				if (x == 'x') {
					gumb.setBackground(Color.PINK);
				}
			}
		});
	}
	
	
	
	
	
}
