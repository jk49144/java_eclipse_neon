package math.arithmetic;

public class Arithmetic {
	
	public static <T extends Number> double addition(T x, T y) {
		return x.doubleValue() + y.doubleValue();
	}
	public static <T extends Number> double substraction(T x, T y) {
		return x.doubleValue() - y.doubleValue();
	} 
	@SafeVarargs
	public static <T extends Number> double sum(T x, T...y) {
		double sum = x.doubleValue();
		for (T t : y) {
			sum += t.doubleValue();
		}
		return sum;
	}
	public static <T extends Number> double multiplication(T x, T y) {
		return x.doubleValue() * y.doubleValue();
	}
	public static <T extends Number> double division(T x, T y) {
		return x.doubleValue() / y.doubleValue();
	}
	@SafeVarargs
	public static <T extends Number> double product(T x, T...y) {
		double product = x.doubleValue();
		for (T t : y) {
			product *= t.doubleValue();
		}
		return product;
	}
	
	public static <T extends Number> double percentageOf(T x, T y) {
		return x.doubleValue() / y.doubleValue() * 100;
	}
	
}
