package seminar;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class Window extends JFrame{

	
	public static void main(String[] args) {
		Window window = new Window();
		window.setVisible(true);
		window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		window.setSize(600, 400);
		window.setTitle("Tic-tac-toe");
	}
}
